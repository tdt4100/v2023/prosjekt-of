# Eksempelprosjekt for TDT4100 prosjekt V2023

Dette repoet er et eksempelprosjekt for TDT4100 prosjektet våren 2023 som skal arbeides med i øvingsforelesning.

**NB!**
Denne versjonen er utvidet sammenlignet med det vi gjorde i forelesningen, og har blandt annet en historiefunksjon. Det er brukt mer avanserte konsepter her, og det er derfor ikke nødvendig å forstå alt som skjer i koden. Det er heller ikke nødvendig å forstå alt som skjer i koden for å kunne fullføre prosjektet. Prosjektet vi gjennomførte i forelesning er mer enn nok for å få godkjent prosjektet. Dette eksempelprosjektet er ment som en ekstra utfordring for de som ønsker det, eller bare er interessert i å se hvordan man kan gjøre ting litt annerledes.
