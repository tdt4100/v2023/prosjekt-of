package wordle.ui;

import java.time.format.DateTimeFormatter;

import javafx.scene.control.ListCell;
import wordle.game.WordleGameState;

public class WordleGameListCell extends ListCell<WordleGameState> {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void updateItem(WordleGameState item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            this.setText(null);
        } else {
            this.setText(item.getCurrentWord() + " (" + item.getStartTime().format(FORMATTER) + ")");
        }
    }
}
