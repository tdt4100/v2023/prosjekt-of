package wordle.ui;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import wordle.game.FileWordProvider;
import wordle.game.WordleGame;
import wordle.game.WordleHistoryManager;
import wordle.util.StageContainer;
import wordle.util.ViewUtil;

public class WordleController extends StageContainer {

    private static final String STATE_FILE_PATH = "game_state.txt";

    @FXML
    private GridPane grid;

    @FXML
    private TextField guessInputField;

    @FXML
    private Button guessButton;

    @FXML
    private Button newGameButton;

    private WordleGame game;
    private WordleHistoryManager historyManager;

    @FXML
    public void initialize() {
        try {
            this.game = new WordleGame(new FileWordProvider());
            this.historyManager = new WordleHistoryManager();
        } catch (IOException e) {
            e.printStackTrace();
            this.handleCriticalError("Noe gikk galt under oppstart av programmet");
            return;
        }

        try {
            if (Files.exists(Path.of(STATE_FILE_PATH))) {
                this.game.readStateFromFile(STATE_FILE_PATH);
            }
        } catch (IOException e) {
            e.printStackTrace();

            Alert alert = new Alert(AlertType.WARNING, "Kunne ikke lese spillstatus");
            alert.show();
        }

        this.guessInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > WordleGame.WORD_LENGTH) {
                this.guessInputField.setText(oldValue);
                return;
            }

            this.guessButton.setDisable(newValue.length() != WordleGame.WORD_LENGTH);
        });

        this.updateGrid();
    }

    public void setGame(WordleGame game, WordleHistoryManager historyManager) {
        this.game = game;
        this.historyManager = historyManager;

        this.updateGrid();
    }

    @FXML
    public void makeNewGuess() {
        String guess = this.guessInputField.getText();
        if (guess.length() != WordleGame.WORD_LENGTH) {
            this.handleErrorInput("Det er kun lov å skrive inn ord av lengde " + WordleGame.WORD_LENGTH);
            return;
        }

        if (!this.canGuessMore()) {
            this.handleErrorInput("Du kan ikke gjette flere ganger!");
            return;
        }

        this.game.makeGuess(guess);
        this.updateGrid();
        this.guessInputField.setText("");

        this.saveGameState();
    }

    @FXML
    public void newGame() {
        try {
            this.historyManager.saveSnapshot(this.game);
        } catch (IOException e) {
            e.printStackTrace();
        
            this.handleErrorInput("Kunne ikke lagre spillstatushistorikk");
        }

        this.game.newGame();
        this.updateGrid();

        this.saveGameState();
    }

    @FXML
    public void goToHistory() {
        ViewUtil.<HistoryController>switchView("wordle/History.fxml", this.getStage(), controller -> {
            controller.setGame(this.game, this.historyManager);
        });
    }

    private void updateGrid() {
        UIHelper.updateGrid(this.grid, this.game);

        this.newGameButton.setDisable(this.game.getPreviousGuessCount() == 0);

        boolean canGuessMore = this.canGuessMore();
        this.guessInputField.setDisable(!canGuessMore);
        this.guessButton.setDisable(!canGuessMore
                || this.guessInputField.getText().length() != WordleGame.WORD_LENGTH);
    }

    private void handleErrorInput(String message) {
        Alert alert = new Alert(AlertType.WARNING, message);
        alert.show();
    }

    private void handleCriticalError(String message) {
        Alert alert = new Alert(AlertType.ERROR, message);
        alert.showAndWait();
        System.exit(1);
    }

    private boolean canGuessMore() {
        return !this.game.isCorrect() && this.game.getPreviousGuessCount() < WordleGame.MAX_GUESSES;
    }

    private void saveGameState() {
        try {
            this.game.writeStateToFile(STATE_FILE_PATH);
        } catch (IOException e) {
            e.printStackTrace();

            Alert alert = new Alert(AlertType.WARNING, "Kunne ikke lagre spillstatus");
            alert.show();
        }
    }
}
