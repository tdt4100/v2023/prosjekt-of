package wordle.ui;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import wordle.game.WordleGame;
import wordle.game.WordleGameState;
import wordle.game.WordleHistoryManager;
import wordle.util.StageContainer;
import wordle.util.ViewUtil;

public class HistoryController extends StageContainer {

    @FXML
    private GridPane grid;

    @FXML
    private ListView<WordleGameState> snapshotList;

    private WordleGame game;
    private WordleHistoryManager historyManager;

    @FXML
    public void initialize() {
        // This allows us to use the WordleGameState object directly, which simplifies
        // selection display logic
        this.snapshotList.setCellFactory((list) -> new WordleGameListCell());
        this.snapshotList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.updateGrid(newValue);
        });
    }

    @FXML
    public void goBackToGame() {
        ViewUtil.<WordleController>switchView("wordle/App.fxml", this.getStage(), controller -> {
            controller.setGame(this.game, this.historyManager);
        });
    }

    public void setGame(WordleGame game, WordleHistoryManager historyManager) {
        this.game = game;
        this.historyManager = historyManager;

        this.updateGrid(new WordleGameState("error"));
        this.updateSnapshots();
    }

    public void updateGrid(WordleGameState state) {
        UIHelper.updateGrid(grid, state);
    }

    public void updateSnapshots() {
        this.snapshotList.getItems().clear();

        try {
            this.snapshotList.getItems().addAll(this.historyManager.getSnapshots());
        } catch (IOException e) {
            e.printStackTrace();

            Alert alert = new Alert(AlertType.WARNING, "Kunne ikke lese historikk");
            alert.show();
        }
    }
}
