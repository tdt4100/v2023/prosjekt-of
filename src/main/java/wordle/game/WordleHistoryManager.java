package wordle.game;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

/**
 * A class that manages the history of Wordle games. It can save and load
 * snapshots of previous games for later viewing.
 */
public class WordleHistoryManager {

    private static final Path HISTORY_FOLDER = Path.of("history");

    /**
     * Creates a new history manager. If the history folder does not exist, it will
     * be created.
     * 
     * @throws IOException If the history folder cannot be created
     */
    public WordleHistoryManager() throws IOException {
        if (Files.notExists(HISTORY_FOLDER)) {
            Files.createDirectory(HISTORY_FOLDER);
        }
    }

    /**
     * Returns a list of all snapshots that are currently stored in the history
     * folder.
     * <p>
     * Note that if a single snapshot cannot be read, it will be skipped and not
     * included in the list. Stack trace is printed to the console.
     * 
     * @return A list of all snapshots
     * @throws IOException If the history folder cannot be read
     */
    public List<WordleGameState> getSnapshots() throws IOException {
        return Files.list(HISTORY_FOLDER) // Get a list of all files in the history folder
                .filter(Files::isRegularFile) // Only include regular files, not directories
                .filter(path -> path.getFileName().toString().startsWith("wordle-game-")) // Only include files that
                                                                                          // start with "wordle-game-"
                .map(path -> {
                    try {
                        return WordleGameState.readFromFile(path.toString()); // Read the file and create a snapshot
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null; // If the file cannot be read, we still have to return some value so we return
                                     // null
                    }
                })
                .filter(Objects::nonNull) // Remove all null values, i.e. snapshots that could not be read
                .toList();
    }

    /**
     * Returns a snapshot with the given name.
     * 
     * @param name The name of the snapshot
     * @return The snapshot with the given name
     * @throws IOException If the snapshot cannot be read, for example if it does
     *                     not exist
     */
    public WordleGameState getSnapshot(String name) throws IOException {
        return WordleGameState.readFromFile(HISTORY_FOLDER + "/" + name);
    }

    /**
     * Saves a snapshot of the given game state to the history folder. This file is
     * named using the start time of the game.
     * 
     * @param state The state to save
     * @throws IOException If the snapshot cannot be saved
     */
    public void saveSnapshot(WordleGameState state) throws IOException {
        // Using the start time of the game allows us to always get a unique name for
        // the file, and is also a nice way to see when the game was played when just
        // looking at the file name
        String name = "wordle-game-" + state.getStartTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + ".txt";
        state.writeStateToFile(HISTORY_FOLDER + "/" + name);
    }
}
