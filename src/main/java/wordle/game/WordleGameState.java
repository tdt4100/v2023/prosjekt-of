package wordle.game;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import wordle.util.FileHelper;

/**
 * A class that represents the state of a Wordle game. It contains the current
 * word, the previous guesses and the start time of the game.
 * <p>
 * This class is externally immutable, i.e. once it is created, it cannot be
 * modified by an class that is not a subclass. This is done to ensure that the
 * state of the game is not modified by accident.
 * <p>
 * This class also contains a static method that can be used to read a snapshot
 * from a file.
 */
public class WordleGameState {

    // The maximum amount of allowed guesses
    public static final int MAX_GUESSES = 6;
    // The length the words should be
    public static final int WORD_LENGTH = 5;

    /**
     * Returns the game state contained in the given file
     * 
     * @param path The path to the file
     * @return The game state contained in the file
     * @throws IOException If the file cannot be read
     */
    public static WordleGameState readFromFile(String path) throws IOException {
        // Use the private constructor to create a new instance
        WordleGameState state = new WordleGameState();
        // Then read the state from the file into the instance
        state.readStateFromFile(path);
        return state;
    }

    private LocalDateTime startTime;
    private String currentWord;
    private List<LetterColor[]> previousGuesses;

    private WordleGameState() {
        // Special constructor for reading directly from file. We make this private
        // because it is technically an invalid state, so we don't want to allow
        // creation of this externally. We could accept a path as input here but
        // this may be confusing because the other constructor accepts a string, but
        // it is not a path.
    }

    public WordleGameState(String currentWord) {
        this.startTime = LocalDateTime.now();
        this.setCurrentWord(currentWord);
    }

    // A set of protected methods that allow subclasses to modify the state of this
    // otherwise
    // immutable class.

    /**
     * Updates the current correct word to the given word and resets the previous
     * guesses
     * 
     * @param currentWord The new correct word
     * @throws IllegalArgumentException If the given word is null or not the correct
     *                                  length
     */
    protected void setCurrentWord(String currentWord) {
        if (currentWord == null) {
            throw new IllegalArgumentException("Current word cannot be null!");
        }

        // Simple sanity check so that if something goes wrong here we know
        if (currentWord.length() != WORD_LENGTH) {
            throw new IllegalStateException(currentWord + " doesn't have length " + WordleGame.WORD_LENGTH);
        }

        this.currentWord = currentWord;
        this.previousGuesses = new ArrayList<>();
    }

    /**
     * Adds a new guess to the list of previous guesses
     * 
     * @param guess The new guess
     * @throws IllegalArgumentException If the given guess is null or not the
     *                                  correct length
     */
    protected void addGuess(LetterColor[] guess) {
        if (guess == null) {
            throw new IllegalArgumentException("Guess cannot be null!");
        }

        if (guess.length != WORD_LENGTH) {
            throw new IllegalArgumentException("Guess must have length " + WORD_LENGTH);
        }

        this.previousGuesses.add(Arrays.copyOf(guess, guess.length));
    }

    /**
     * 
     * @return The {@link LocalDateTime time} when this game started
     */
    public LocalDateTime getStartTime() {
        return this.startTime;
    }

    /**
     * @return The currently correct word
     */
    public String getCurrentWord() {
        return this.currentWord;
    }

    /**
     * @return The amount of previously submitted guesses
     */
    public int getPreviousGuessCount() {
        return this.previousGuesses.size();
    }

    /**
     * @return If the last guess was correct
     */
    public boolean isCorrect() {
        if (this.getPreviousGuessCount() == 0) {
            return false;
        }

        int previousGuessIndex = this.getPreviousGuessCount() - 1;
        LetterColor[] previousGuess = this.getGuess(previousGuessIndex);
        return Arrays.stream(previousGuess).allMatch(guess -> guess.getColor() == LetterColor.CORRECT);
    }

    /**
     * Gets the data for the guess with the given index
     *
     * @param index The index of the guess
     * @return The data for the guess
     */
    public LetterColor[] getGuess(int index) {
        if (index < 0 || index >= this.getPreviousGuessCount()) {
            throw new IllegalArgumentException("Guess out of bounds!");
        }

        LetterColor[] guess = this.previousGuesses.get(index);
        return Arrays.copyOf(guess, guess.length);
    }

    /**
     * Saves the current state of the game to the given file
     *
     * @param path The path to save the state to
     * @throws IOException If an error occurs while writing the file
     */
    public void readStateFromFile(String path) throws IOException {
        List<String> lines = FileHelper.readLines(path, false);

        if (lines.size() < 2) {
            throw new IOException("Invalid file format!");
        }

        LocalDateTime startTime = LocalDateTime.parse(lines.get(0), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        String currentWord = lines.get(1);
        List<LetterColor[]> guesses = new ArrayList<>();

        for (int i = 2; i < lines.size(); i++) {
            String line = lines.get(i);
            String[] parts = line.split(",");

            if (parts.length != WORD_LENGTH) {
                throw new IOException("Invalid word length for word " + line);
            }

            LetterColor[] colors = new LetterColor[WORD_LENGTH];
            for (int j = 0; j < parts.length; j++) {
                String part = parts[j];

                char letter = part.charAt(0);
                char color = part.charAt(1);

                colors[j] = new LetterColor(letter, color);
            }

            guesses.add(colors);
        }

        this.startTime = startTime;
        this.currentWord = currentWord;
        this.previousGuesses = guesses;
    }

    /**
     * Write the game state to file
     * 
     * @param path The path of the file to write to
     * @throws IOException If an error occurs while writing to the file
     */
    public void writeStateToFile(String path) throws IOException {
        List<String> lines = new ArrayList<>();
        lines.add(this.getStartTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        lines.add(this.currentWord);

        for (LetterColor[] guess : this.previousGuesses) {
            StringBuilder builder = new StringBuilder();

            for (LetterColor letter : guess) {
                builder.append(",").append(letter.getLetter()).append(letter.getColor());
            }

            lines.add(builder.substring(1));
        }

        FileHelper.writeLines(path, lines);
    }

    /**
     * Prints the current state of the game to the console
     */
    public void printState() {
        System.out.println("Correct word: " + this.currentWord);

        for (LetterColor[] guess : this.previousGuesses) {
            StringBuilder builder = new StringBuilder();

            for (LetterColor letter : guess) {
                builder.append(" ").append(letter.getLetter()).append(letter.getColor());
            }

            System.out.println(builder.substring(1));
        }
    }
}
