package wordle.game;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import wordle.util.FileHelper;

public class FileWordProvider implements WordProvider {

    private final Random random;
    private List<String> words;

    public FileWordProvider() throws IOException {
        this("/wordle/words.txt", true);
    }

    public FileWordProvider(String path, boolean resource) throws IOException {
        this.random = new Random();
        this.words = FileHelper.readLines(path, resource);
    }

    public List<String> getWords() {
        return words;
    }

    @Override
    public String getNextWord() {
        return words.get(random.nextInt(words.size()));
    }
}
