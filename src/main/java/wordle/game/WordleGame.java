package wordle.game;

import java.util.Arrays;

/**
 * Wordle game logic
 */
public class WordleGame extends WordleGameState {

    // Constant we can use as a placeholder for a letter when it is used, so
    // we know not to use it again in the calculations.
    private static final char USED_LETTER = 0;

    private final WordProvider provider;

    /**
     * Creates a new wordle game logic controller using the given
     * {@link WordProvider word provider}.
     *
     * @param provider The {@link WordProvider provider} to get words from
     */
    public WordleGame(WordProvider provider) {
        super(provider.getNextWord().toUpperCase());
        this.provider = provider;
    }

    /**
     * Starts a new game with a new word and a new set of guesses
     */
    public void newGame() {
        this.setCurrentWord(this.provider.getNextWord().toUpperCase());
    }

    /**
     * Makes a new guess, and adds it to the previous guesses
     *
     * @param guess The new guess word. Case-insensitive.
     * @return The coloring for the given guess
     */
    public LetterColor[] makeGuess(String guess) {
        if (guess == null || guess.length() != WORD_LENGTH) {
            throw new IllegalArgumentException("Invalid guess!");
        }

        guess = guess.toUpperCase();
        if (this.getPreviousGuessCount() >= MAX_GUESSES) {
            throw new IllegalStateException("Can't make more guesses");
        }

        if (this.isCorrect()) {
            throw new IllegalStateException("Previous guess was already correct");
        }

        char[] available = this.getCurrentWord().toCharArray();
        LetterColor[] result = new LetterColor[WORD_LENGTH];

        // First pass: Find letters in the correct place
        for (int i = 0; i < WORD_LENGTH; i++) {
            char guessChar = guess.charAt(i);
            char wordChar = available[i];

            // Check if it is a correct guess. If so, we mark it as green
            if (guessChar == wordChar) {
                result[i] = new LetterColor(guessChar, LetterColor.CORRECT);
                // Mark this letter as used, so that we don't match another letter
                // in the guess to it later
                available[i] = USED_LETTER;
            }
        }

        // Second pass: Find letters in the wrong place, but that exist in the word
        for (int i = 0; i < WORD_LENGTH; i++) {
            // Ignore already set values from the previous pass
            if (result[i] != null) {
                continue;
            }

            char guessChar = guess.charAt(i);

            // We then check every other letter for a match
            for (int j = 0; j < WORD_LENGTH; j++) {
                char wordChar = available[j];

                // If the match exists, then this is a correct letter, but in the wrong
                // position. We match on the first found.
                if (guessChar == wordChar) {
                    result[i] = new LetterColor(guessChar, LetterColor.INCORRECT_POSITION);
                    available[j] = USED_LETTER;
                    break;
                }
            }
        }

        // Third pass: Mark the remaining letters as incorrect
        for (int i = 0; i < WORD_LENGTH; i++) {
            // Ignore already set values from the previous passes
            if (result[i] != null) {
                continue;
            }

            char guessChar = guess.charAt(i);
            result[i] = new LetterColor(guessChar, LetterColor.WRONG);
        }

        // Add this guess to the previous guesses
        this.addGuess(result);
        return Arrays.copyOf(result, result.length);
    }
}
