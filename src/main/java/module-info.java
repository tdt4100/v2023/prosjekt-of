open module wordle {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;

    // Trengs for å kunne bruke PopupUtil.java
    requires java.desktop;
}
